<?php

namespace app\v1;

use yii\web\Response;

/**
 * v1 module definition class
 */
class V1 extends \yii\base\Module
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['cors']=[
            'class' => \yii\filters\Cors::className(),
            'cors'=>[
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'OPTIONS', 'HEAD'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\v1\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        \Yii::$app->user->enableSession = false;
        parent::init();
    }
}
