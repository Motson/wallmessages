<?php

namespace app\v1\controllers;

use app\models\Message;
use app\models\User;
use yii\web\Controller;

class MessageController extends Controller
{
    /**
     * @return string
     */
    public function actionList()
    {
        $messages = (new \yii\db\Query())
            ->select(['m.id', 'm.user_id', 'u.username', 'm.message', 'm.created_at'])
            ->from('{{message}} m')
            ->leftJoin('{{user}} u', 'u.id = m.user_id')
            ->limit(50)
            ->orderBy('m.created_at DESC')
            ->all();
        foreach ($messages as &$message) {
            $message['edit'] = false;
        }
        return [
            'success'=>true,
            'error'=>'',
            'messages'=>$messages
        ];
    }

    public function actionAdd() {
        $success = false;
        $errorMessage = '';
        $input = json_decode(file_get_contents('php://input'), true);
        if(!empty($input['auth_key'])) {
            $userModel = User::findIdentityByAccessToken($input['auth_key']);
            if($userModel) {
                $messageModel = new Message();
                $messageModel->setScenario('add');
                $messageModel->user_id = $userModel->id;
                $messageModel->message = isset($input['newMessage']) ? trim($input['newMessage']) : null;
                if($messageModel->save()) {
                    $success = true;
                } elseif( !empty($messageModel->errors['message']) ) {
                    $errorMessage = implode(', ', $messageModel->errors['message']);
                } else {
                    $errorMessage = 'Не удалось добавить сообщение';
                }
            } else {
                $errorMessage = 'Необходима авторизация';
            }
        }

        return [
            'success'=>$success,
            'error'=>$errorMessage,
        ];
    }

    public function actionUpdate() {
        $success = false;
        $errorMessage = '';
        $input = json_decode(file_get_contents('php://input'), true);
        if(!empty($input['auth_key']) and !empty($input['message'])) {
            $userModel = User::findIdentityByAccessToken($input['auth_key']);
            if($userModel) {
                $inputMessage = $input['message'];
                if($inputMessage) {
                    $message = Message::findOne(['id'=>$inputMessage['id'], 'user_id'=>$userModel->id]);
                    $message->message = $inputMessage['message'];
                    if($message and $message->save()) {
                        $success = true;
                    } else {
                        $errorMessage = 'Не удалось обновить сообщение';
                    }
                }
            }
        } else {
            $errorMessage = 'Неверные входные данные';
        }

        return [
            'success'=>$success,
            'error'=>$errorMessage,
        ];
    }

    public function actionRemove() {
        $success = false;
        $errorMessage = '';
        $input = json_decode(file_get_contents('php://input'), true);
        if(!empty($input['auth_key']) and !empty($input['id'])) {
            $userModel = User::findIdentityByAccessToken($input['auth_key']);
            if($userModel) {
                $message = Message::findOne(['id'=>$input['id'], 'user_id'=>$userModel->id]);
                if($message and $message->delete()) {
                    $success = true;
                } else {
                    $errorMessage = 'Не удалось удалить сообщение';
                }
            }
        } else {
            $errorMessage = 'Необходима авторизация';
        }
        return [
            'success'=>$success,
            'error'=>$errorMessage,
        ];
    }
}
