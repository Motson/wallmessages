<?php

namespace app\v1\controllers;
use app\models\LoginForm;
use app\models\User;
use yii\web\Controller;

class UserController extends Controller
{
    public function actionIndex() {
        $input = json_decode(file_get_contents('php://input'), true);
        $user = new User();
        $inputKey = $input['auth_key'];
        $user->auth_key = $inputKey;
        $errorMessage = '';
        if($user = User::findIdentityByAccessToken($inputKey)) {
            $userInfo = [
                'id'=>$user->getId(),
                'isGuest'=>false,
                'username'=>$user->username
            ];
        } else {
            $userInfo = [
                'id'=>null,
                'isGuest'=>true,
                'username'=>'Guest',
            ];
        }
        return [
            'success'=>!empty($userInfo),
            'errorMessage'=>$errorMessage,
            'userInfo'=>$userInfo,
        ];
    }

    public function actionLogin() {

        $errorMessage = '';
        $success = false;
        $auth_key = '';
        $input = json_decode(file_get_contents('php://input'), true);
        if ($input) {
            $model = new LoginForm();
            $model->setAttributes($input);
            if ($model->login() and $model->getUser()->auth_key) {
                $auth_key = $model->getUser()->auth_key;
                $success = true;
            } else {
                $errorMessage = 'Вход в систему с указанными данными невозможен';
            }
        } else {
            $errorMessage = 'Пользователь уже авторизован';
        }
        return [
            'success'=>$success,
            'error'=>$errorMessage,
            'auth_key'=>$auth_key,
        ];
    }

    public function actionRegistration() {
        $success = false;
        $errorMessage = '';
        $auth_key = '';
        $formErrors = [];
        $_POST = json_decode(file_get_contents('php://input'), true);
        $model = new User();
        $model->setScenario('registration');
        $model->setAttributes($_POST);

        if ($model->validate()) {
            $model->setPassword($model->password1);
            $model->generateAuthKey();
            if($model->save()) {
                $auth_key = $model->auth_key;
                $success = true;
            }
        } else {
            if($model->errors) {
                foreach ($model->errors as $errorField => $error) {
                    $formErrors[$errorField] = implode(', ', $error);
                }
            }
            $errorMessage = 'Не удалось зарегистрироваться';
        }

        return [
            'success'=>$success,
            'error'=>$errorMessage,
            'auth_key'=>$auth_key,
            'formErrors'=>$formErrors,
        ];
    }
}
