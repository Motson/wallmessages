<?php

use yii\db\Migration;

class m170413_230417_init extends Migration
{
    public function safeUp() {
        $this->createTable('{{user}}', [
            'id'=>$this->primaryKey(),
            'username'=>$this->string(255)->comment('Логин'),
            'password_hash'=>$this->string(60)->comment('Хэш пароля'),
            'auth_key'=>$this->string(60)->comment('Ключ авторизации'),
            'created_at'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->comment('Дата обновления'),
            'updated_at'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')->comment('Дата создания'),
        ]);
        $this->createIndex('username_index', '{{user}}', 'username');

        $this->createTable('{{message}}', [
            'id'=>$this->primaryKey(),
            'user_id'=>$this->integer()->comment('ИД пользователя'),
            'message'=>$this->text()->comment('Текст сообщения'),
            'created_at'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->comment('Дата обновления'),
            'updated_at'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')->comment('Дата создания'),
        ]);
        $this->createIndex('user_id_index', '{{message}}', 'user_id');
        $this->addForeignKey('user_id_fk', '{{message}}', 'user_id', '{{user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function safeDown() {
        $this->dropTable('{{message}}');
        $this->dropTable('{{user}}');
    }
}
