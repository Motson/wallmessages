Сервис “Стена сообщений”
============================
Репозиторий состоит из:
1. php сервиса, написан на Yii2
2. фронэнд приложения, написан на Vue2

ТРЕБОВАНИЯ
------------

PHP >=5.4.0, MySQL 5.5


НАСТРОЙКА БД
-------------

Отредактируйте файл`config/db.php`:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=wallmessages',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```

ИСПОЛЬЗОВАНИЕ
------------

- шаг 1: composer install
- шаг 2: php init
- шаг 3: php yii migrate
- шаг 4: cd /web
- шаг 5: php -S localhost:8081
- шаг 6: npm install
- шаг 7: npm run dev (или build)
- шаг 8: открыть сайт '<http://localhost:8080>'


