<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * RegistrationForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegistrationForm extends User
{
    public $username = '';
    public $password = '';
    public $password2 = '';

//    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password', 'password2'], 'required', 'on'=>'registration'],
            [['username', 'password'], 'safe', 'on'=>'registration'],
            [['password', 'password2'], 'validatePassword', 'on'=>'registration'],
            [['password', 'password2'], 'string', 'length'=>[4,20], 'on'=>'registration'],
            ['username', 'unique', 'on'=>'registration'],
        ];
    }
}
