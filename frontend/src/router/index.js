import Vue from 'vue'
import Router from 'vue-router'
import Messages from '@/components/Messages'
import Login from '@/components/Login'
import Registration from '@/components/Registration'
import RegistrationSuccess from '@/components/RegistrationSuccess'


Vue.use(Router);

export default new Router({
  routes: [
    { path: '/', component: Messages},
    { path: '/login', component: Login},
    { path: '/registration', component: Registration },
    { path: '/registration-success', component: RegistrationSuccess }
  ],
  // mode: 'history'
});