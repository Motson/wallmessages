Сервис “Стена сообщений”
============================
Репозиторий состоит из:

1.php сервиса, написан на Yii2. Расположено в папке api.

2.фронэнд приложения, написан на Vue2.  Расположено в папке frontend.

ТРЕБОВАНИЯ
------------

PHP >=5.4.0, MySQL 5.5


ИСПОЛЬЗОВАНИЕ
------------


### Backend

1.Перейти в папку api

```php
cd api
```

2.Установить зависимости

```php
composer global require "fxp/composer-asset-plugin:^1.2.0"
composer update
```
3.Настроить подключение к БД в файле api\config\db.php

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=wallmessages',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];

```

4.Запустить миграцию для создания структуры БД

```php
yii migrate
```

5.Настроить хост в http сервере с точкой входа web, либо запустить встроенный дев сервер: 

```php
cd web
php -S localhost:8081
```

### Frontend

1.Перейти в папку frontend

```php
cd frontend
```
2.Установить зависимости

```php
npm install

```
3.Настроить url для подключение к api в файле frontend\src\main.js

```js
Vue.http.options.root = 'http://localhost:8081'
```

4.Запустить втроенный дев-сервер/собрать проект

```php
npm run dev (или build)
```